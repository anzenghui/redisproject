module redisProject

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.16.0
	github.com/miekg/dns v1.1.22 // indirect
	golang.org/x/net v0.0.0-20191028085509-fe3aa8a45271 // indirect
)
