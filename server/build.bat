protoc --proto_path=./ --micro_out=./build/ --go_out=./build/ ./proto/User.proto
protoc --proto_path=./ --micro_out=./build/ --go_out=./build/ ./proto/Chat.proto
protoc --proto_path=./ --micro_out=./build/ --go_out=./build/ ./proto/Event.proto